(function($){
  var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
  $(document).ready(initPage);
  function initPage() {
    tab_effect();
    $(window).bind("scroll", fixed_header);
    $("#members .member-intro a").bind("click", read_more);
    $(".case_block a").bind("click", case_popup);
    $("#case_popup .close-btn a").bind("click", close_case_popup);
    $(".goTop a").bind("click", gotop);
    $(".news-toggle a").bind("click", news_filter);
  }

  function tab_effect() {
    var _showTab = 0;
    $('.content_tabs').each(function(){
      var $tab = $(this);
      var $defaultLi = $('ul.tabs li', $tab).eq(_showTab).addClass('active');
      $($defaultLi.find('a').attr('href')).siblings().hide();
      $('ul.tabs li', $tab).click(function() {
        var $this = $(this),
          _clickTab = $this.find('a').attr('href');
        $this.addClass('active').siblings('.active').removeClass('active');
        $(_clickTab).stop(false, true).fadeIn().siblings().hide();
        return false;
      }).find('a').focus(function(){
        this.blur();
      });
    });
  }

  function read_more() {
    $all_p = $("#members .member-intro p");
    $p = $(this).prevUntil();
    if ($p.hasClass('hide')){
      $all_p.addClass('hide');
      $p.removeClass('hide');
    }else{
      $p.addClass('hide');
    }
  }

  function case_popup(){
    var idx = $(this).attr('data-role');
    // console.log(idx);
    $('#case_popup').show();
    $('.bxslider').bxSlider({
      infiniteLoop: false,
      controls: false
    });
    $('body').addClass('overflow_hidden');
  }

  function close_case_popup(){
    $('#case_popup').hide();
    $('body').removeClass('overflow_hidden');
    return false;
  }

  function gotop(){
    $body.animate({scrollTop: 0}, 700, 'easeInOutCirc');
    return false;
  }

  function fixed_header(){
    if($(window).scrollTop() > 0){
      $("#header").addClass('fixed');
    } else {
      $("#header").removeClass('fixed');  
    }
  }

  function news_filter(){
    if($(".news-filter").hasClass('open')){
      $(".news-filter").removeClass('open');
    } else {
      $(".news-filter").addClass('open');
    }
  }

})(jQuery);